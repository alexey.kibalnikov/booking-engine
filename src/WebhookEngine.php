<?php
/**
 * WebhookEngine.php
 * @author Alexey Kibalnikov (alexey.kibalnikov@gmail.com)
 * @copyright (c) iWeekender 2019
 * @package BookingEngine
 */

namespace iWeekender\BookingEngine;

use iWeekender\Contract\Booking\WebhookStatusEnumInterface;
use iWeekender\Contract\DataBase\MySQLAdapterInterface;
use iWeekender\Contract\IMS\FlightSupplierEnumInterface;
use iWeekender\FlightSuppliers\Sdk\TequilaByKiwi\webhooks\WebhookRequest;
use iWeekender\Exceptions\ExceptionHandler;
use Doctrine\DBAL\DBALException;
use Psr\Log\LoggerInterface;
use Throwable;

class WebhookEngine
{
    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var SQLHelper
     */
    private $sqlHelper;

    public function __construct(
        LoggerInterface $logger,
        MySQLAdapterInterface $mySQLAdapter
    ) {
        $this->logger = $logger;
        $this->sqlHelper = new SQLHelper($mySQLAdapter);
    }

    public function received(array $request): bool {
        try {
            $webhookRequest = new WebhookRequest();
            $webhookRequest->loadToObject($request);

            $this->sqlHelper->insertWebhooksTbk(
                $webhookRequest->getBid(),
                $webhookRequest->getType(),
                $webhookRequest->getTrigger(),
                $request
            );
        } catch (Throwable $t) {
            new ExceptionHandler($this->logger, $t);
        } finally {
            return true;
        }
    }

    /**
     * @throws DBALException
     */
    public function updateReceivedWebhooks(): void {
        $this->sqlHelper->updateReceivedWebhooksTbk();
    }

    /**
     * @param string $supplier
     * @param int $bookingID
     * @return string|null
     * @throws DBALException
     */
    public function getWebhookJson(string $supplier, int $bookingID): ?string {
        if ($supplier === FlightSupplierEnumInterface::TEQUILA_BY_KIWI) {
            return $this->sqlHelper->getWebhooksJsonTbk($bookingID);
        } else {
            return null;
        }
    }

    /**
     * @param string $supplier
     * @param int $bookingID
     * @throws DBALException
     */
    public function setCompleted(string $supplier, int $bookingID) {
        if ($supplier === FlightSupplierEnumInterface::TEQUILA_BY_KIWI) {
            $this->sqlHelper->updateWebhooksTbk($bookingID, WebhookStatusEnumInterface::COMPLETED);
        }
    }
}
