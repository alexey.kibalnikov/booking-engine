<?php
/**
 * SQLHelper.php
 * @author Alexey Kibalnikov (alexey.kibalnikov@gmail.com)
 * @copyright (c) iWeekender 2019
 * @package BookingEngine
 */

namespace iWeekender\BookingEngine;

use iWeekender\Contract\Booking\BookingStatusEnumInterface;
use iWeekender\Contract\Booking\OrderStatusEnumInterface;
use iWeekender\Contract\Booking\WebhookStatusEnumInterface;
use iWeekender\Contract\DataBase\MySQLAdapterInterface;
use iWeekender\Exceptions\IWException;
use function iWeekender\Contract\arrayToJson;
use Doctrine\DBAL\DBALException;

/**
 * Contains all methods of working with databases. Working with the database directly ideologically incorrect.
 */
class SQLHelper
{
    /**
     * @var MySQLAdapterInterface
     */
    private $mySQLAdapter;

    /**
     * SQLHelper constructor.
     * @param MySQLAdapterInterface $mySQLAdapter
     */
    public function __construct(MySQLAdapterInterface $mySQLAdapter) {
        $this->mySQLAdapter = $mySQLAdapter;
    }

    /**
     * @param string $serpID
     * @return bool
     * @throws DBALException
     */
    public function isStartOfBooking(string $serpID): bool {
        $query = "CALL SELECT_ORDER_DETAILS(:serp_id)";
        $stmt = $this->mySQLAdapter->executeQuery($query, ['serp_id' => $serpID]);
        return $stmt->rowCount() == 0;
    }

    /**
     * @param string $orderID
     * @param string $serpID
     * @param string $json
     * @throws DBALException
     */
    public function insertOrderDetails(string $orderID, string $serpID, string $json): void {
        $query = "CALL INSERT_ORDER_DETAILS(:order_id, :serp_id, :status, :params)";
        $this->mySQLAdapter->executeQuery($query, [
            'order_id' => $orderID,
            'serp_id'  => $serpID,
            'status'   => OrderStatusEnumInterface::IN_PROCESS,
            'params'   => $json
        ]);
    }

    /**
     * @param int $id
     * @param string $status
     * @throws DBALException
     */
    public function updateOrderDetails(int $id, string $status): void {
        $query = "CALL UPDATE_ORDER_DETAILS(:id, :status)";
        $this->mySQLAdapter->executeQuery($query, [
            'id'     => $id,
            'status' => $status
        ]);
    }

    /**
     * @param string $serpID
     * @param OrderItem $orderItem
     * @throws DBALException
     * @throws IWException
     */
    public function loadDataFromOrderDetails(string $serpID, OrderItem $orderItem): void {
        $query = "CALL SELECT_ORDER_DETAILS(:serp_id)";
        $row = $this->mySQLAdapter->executeQueryFetch($query, ['serp_id' => $serpID]);
        $orderItem->loadDataFromOrderDetails($row['id'], $row['params']);
    }

    /**
     * @param string $serpID
     * @return array | bool
     * @throws DBALException
     */
    public function getOrderDetailsStatus(string $serpID) {
        $query = "CALL SELECT_ORDER_DETAILS_STATUS(:serp_id)";
        return $this->mySQLAdapter->executeQueryFetch($query, ['serp_id' => $serpID]);
    }

    /**
     * @param int $orderDetailsID
     * @param string $serpID
     * @param string $action
     * @param bool $checked
     * @param string $externalID
     * @param $bookingToken
     * @throws DBALException
     */
    public function insertBooking(int $orderDetailsID, string $serpID, string $action, bool $checked, string $externalID, $bookingToken): void {
        $query = "CALL INSERT_BOOKING(:order_details_id, :serp_id, :action, :status, :external_id, :booking_token)";
        $this->mySQLAdapter->executeQuery($query, [
            'order_details_id' => $orderDetailsID,
            'serp_id'          => $serpID,
            'action'           => $action,
            'status'           => $checked ? BookingStatusEnumInterface::SUCCESS : BookingStatusEnumInterface::FAILURE,
            'external_id'      => $externalID,
            'booking_token'    => $bookingToken
        ]);
    }

    /**
     * @param int $id
     * @param string $action
     * @param bool $checked
     * @throws DBALException
     */
    public function updateBooking(int $id, string $action, bool $checked): void {
        $query = "CALL UPDATE_BOOKING(:id, :action, :status)";
        $this->mySQLAdapter->executeQuery($query, [
            'id'     => $id,
            'action' => $action,
            'status' => $checked ? BookingStatusEnumInterface::SUCCESS : BookingStatusEnumInterface::FAILURE
        ]);
    }

    /**
     * @param int $id
     * @param string $externalWebhookId
     * @throws DBALException
     */
    public function updateBookingWebhookId(int $id, ?string $externalWebhookId): void {
        $query = "CALL UPDATE_BOOKING_WEBHOOK_ID(:id, :external_webhook_id)";
        $this->mySQLAdapter->executeQuery($query, [
            'id'     => $id,
            'external_webhook_id' => $externalWebhookId
        ]);
    }

    /**
     * @param string $serpID
     * @param OrderItem $orderItem
     * @throws DBALException
     */
    public function loadDataFromBooking(string $serpID, OrderItem $orderItem): void {
        $query = "CALL SELECT_BOOKING(:serp_id)";
        $row = $this->mySQLAdapter->executeQueryFetch($query, ['serp_id' => $serpID]);
        $orderItem->loadDataFromBooking($row['id'], $row['action'], $row['status'], $row['external_id'], $row['booking_token']);
    }

    /**
     * @param int $bookingID
     * @param string $serpID
     * @param string $action
     * @param bool $checked
     * @param array $response
     * @throws DBALException
     */
    public function insertBookingResponse(int $bookingID, string $serpID, string $action, bool $checked, array $response): void {
        $query = "CALL INSERT_BOOKING_RESPONSE(:booking_id, :serp_id, :action, :status, :json)";
        $this->mySQLAdapter->executeQuery($query, [
            'booking_id' => $bookingID,
            'serp_id'    => $serpID,
            'action'     => $action,
            'status'     => $checked ? BookingStatusEnumInterface::SUCCESS : BookingStatusEnumInterface::FAILURE,
            'json'       => arrayToJson($response)
        ]);
    }

    /**
     * @param string $serpID
     * @param string $action
     * @return string
     * @throws DBALException
     */
    public function getResponseJson(string $serpID, string $action): ?string {
        $query = "CALL SELECT_BOOKING_RESPONSE(:serp_id, :action)";
        $row = $this->mySQLAdapter->executeQueryFetch($query, [
            'serp_id' => $serpID,
            'action'  => $action
        ]);

        if (isset($row['json'])) {
            return $row['json'];
        }
        return null;
    }

    /**
     * @param string $serpID
     * @return array | bool
     * @throws DBALException
     */
    public function getBookingStatuses(string $serpID) {
        $query = "CALL SELECT_BOOKING_RESPONSE_STATUS(:serp_id)";
        return $this->mySQLAdapter->executeQueryFetchAll($query, ['serp_id' => $serpID]);
    }

    /**
     * @param string $serpID
     * @return array | bool
     * @throws DBALException
     */
    public function getSupplierResponse(string $serpID) {
        $query = "CALL SELECT_BOOKING_RESPONSE_LAST_JSON(:serp_id)";
        return $this->mySQLAdapter->executeQueryFetch($query, ['serp_id' => $serpID]);
    }

    /**
     * @return array
     * @throws DBALException
     */
    public function getBookingTasks(): array {
        $query = "CALL SELECT_ORDER_DETAILS_TASKS()";
        return $this->mySQLAdapter->executeQueryFetchAll($query, []);
    }

    /**
     * @param string $bid
     * @param string $type
     * @param string $trigger
     * @param array $request
     * @throws DBALException
     */
    public function insertWebhooksTbk(string $bid, string $type, string $trigger, array $request): void {
        $query = "CALL INSERT_WEBHOOKS_TBK(:status, :bid, :type, :trigger, :json)";
        $this->mySQLAdapter->executeQuery($query, [
            'status'  => WebhookStatusEnumInterface::RECEIVED,
            'bid'     => $bid,
            'type'    => $type,
            'trigger' => $trigger,
            'json'    => arrayToJson($request)
        ]);
    }

    /**
     * @throws DBALException
     */
    public function updateReceivedWebhooksTbk(): void {
        $query = "CALL UPDATE_RECEIVED_WEBHOOKS_TBK()";
        $this->mySQLAdapter->executeQuery($query, []);
    }

    /**
     * @param int $bookingID
     * @return string|null
     * @throws DBALException
     */
    public function getWebhooksJsonTbk(int $bookingID): ?string {
        $query = "CALL SELECT_WEBHOOKS_TBK(:booking_id)";
        $row = $this->mySQLAdapter->executeQueryFetch($query, ['booking_id' => $bookingID]);
        if (isset($row['json'])) {
            return $row['json'];
        }
        return null;
    }

    /**
     * @param int $bookingID
     * @param string $status
     * @throws DBALException
     */
    public function updateWebhooksTbk(int $bookingID, string $status): void {
        $query = "CALL UPDATE_WEBHOOKS_TBK(:booking_id, :status)";
        $this->mySQLAdapter->executeQuery($query, [
            'booking_id' => $bookingID,
            'status' => $status
        ]);
    }
}
