<?php
/**
 * BookingEngine.php
 * @author Alexey Kibalnikov (alexey.kibalnikov@gmail.com)
 * @copyright (c) iWeekender 2019
 * @package BookingEngine
 */

namespace iWeekender\BookingEngine;

use iWeekender\BookingEngine\Exceptions\ExceptionMessagesInterface as EM;
use iWeekender\BookingEngine\Exceptions\ExceptionValidationMessagesInterface as EVM;
use iWeekender\Contract\DataBase\MySQLAdapterInterface;
use iWeekender\Contract\IMS\BookingWorkflowFlightInterface as BWF;
//use iWeekender\DataModels\Flight\Transformation\TransformationToClientModel;
use iWeekender\Contract\Request\Search\SearchRequestInterface as SE;
use iWeekender\Exceptions\ExceptionHandler;
use iWeekender\FlightSuppliers\Adapters\AviacenterBookingAdapter;
use iWeekender\FlightSuppliers\Adapters\TequilaByKiwiBookingAdapter;
use iWeekender\Contract\Request\Booking\ApiBookingRequestInterface;
use iWeekender\Contract\Request\Booking\ApiGetSupplierResponseRequestInterface;
use iWeekender\Contract\Request\Booking\ApiGetStatusesRequestInterface;
use iWeekender\IMS\IMS;
use iWeekender\Request\Search\ApiGetRateBySerpIdRequest;
use iWeekender\Contract\Booking\Flight\FlightBookingInterface;
use iWeekender\Contract\Booking\BookingStatusEnumInterface as BSE;
use iWeekender\Contract\Booking\BookingActionEnumInterface as BAE;
use iWeekender\Contract\Booking\OrderStatusEnumInterface as OSE;
use iWeekender\Contract\Configs\DataModelConfigInterface;
use iWeekender\Contract\Configs\SuppliersApiConfigInterface;
use iWeekender\Contract\Configs\ServicesConfigInterface;
use iWeekender\Contract\IMS\FlightSupplierEnumInterface;
use iWeekender\ServicesSDK\Client\ClientAPI;
use iWeekender\ServicesSDK\Search\Flight\FlightSearchAPI;
use iWeekender\Exceptions\IWException;
use iWeekender\Exceptions\IWExceptionValidation;
use iWeekender\Exceptions\IWExceptionConfiguration;
use iWeekender\Utils\Response\ResponseHelper;
use function iWeekender\Contract\arrayToJson;
use function iWeekender\Contract\jsonToArray;
use Doctrine\DBAL\DBALException;
use GuzzleHttp\Exception\GuzzleException;
use Psr\Log\LoggerInterface;

/**
 * Class BookingEngine
 */
class BookingEngine
{
    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var SQLHelper
     */
    private $sqlHelper;

    /**
     * @var SuppliersApiConfigInterface
     */
    private $suppliersApiConfig;

    /**
     * @var DataModelConfigInterface
     */
    private $dataModelConfig;

    /**
     * @var ServicesConfigInterface
     */
    private $servicesConfig;

    /**
     * @var WebhookEngine
     */
    private $webhookEngine;

    public function __construct(
        LoggerInterface $logger,
        MySQLAdapterInterface $mySQLAdapter,
        DataModelConfigInterface $dataModelConfig,
        SuppliersApiConfigInterface $suppliersApiConfig,
        ServicesConfigInterface $servicesConfig
    ) {
        $this->logger = $logger;
        $this->sqlHelper = new SQLHelper($mySQLAdapter);

        $this->suppliersApiConfig = $suppliersApiConfig;
        $this->dataModelConfig = $dataModelConfig;
        $this->servicesConfig = $servicesConfig;
        $this->webhookEngine = new WebhookEngine($logger, $mySQLAdapter);
    }

    public function logging(string $serpID, string $action, string $message): void {
        $message = sprintf(
            "(serpID: %s) action: %s %s",
            $serpID,
            $action,
            $message
        );
        $this->logger->info($message);
    }

    /**
     * @param ApiBookingRequestInterface $apiBookingRequest
     * @return bool
     * @throws DBALException
     * @throws GuzzleException
     * @throws IWException
     * @throws IWExceptionValidation
     */
    public function createBookingProcess(ApiBookingRequestInterface $apiBookingRequest): bool {
        $orderID = $apiBookingRequest->getOrderId();
        $serpID = $apiBookingRequest->getSerpId();
        $passengers = $apiBookingRequest->getPassengers();

        $isStartOfBooking = $this->sqlHelper->isStartOfBooking($serpID);
        if (!$isStartOfBooking) {
            throw new IWExceptionValidation(sprintf(EVM::MES_CREATE_NOT_UNIQUE_SERP_ID, $serpID));
        }

        $orderItem = new OrderItem($orderID, $serpID);
        $this->createFlightBookingProcess($orderItem, $passengers);

        return true;
    }

    /**
     * @param ApiGetStatusesRequestInterface $apiGetStatusesRequest
     * @return array
     * @throws DBALException
     * @throws IWExceptionConfiguration
     * @throws IWExceptionValidation
     */
    public function getStatuses(ApiGetStatusesRequestInterface $apiGetStatusesRequest): array {
        $result = [];
        $serpID = $apiGetStatusesRequest->getSerpId();

        $orderDetailsStatus = $this->sqlHelper->getOrderDetailsStatus($serpID);
        if ($orderDetailsStatus === false) {
            throw new IWExceptionValidation(sprintf(EVM::MES_SERP_ID_HAS_NEVER_BEEN_BOOKED, $serpID));
        }
        $result['status'] = $orderDetailsStatus['status'];
        $result['workflow'] = [];

        $parsed = ResponseHelper::parseSerpId($serpID);
        $supplier = $parsed[SE::SUPPLIER];
        $bookingWorkflow = IMS::getBookingWorkflowFlight($supplier);
        foreach ($bookingWorkflow as $key => $value) {
            $result['workflow'][$key] = ['status' => null, 'datetime' => null];
        }

        $bookingStatuses = $this->sqlHelper->getBookingStatuses($serpID);
        foreach ($bookingStatuses as $row) {
            $result['workflow'][$row['action']]['status'] = $row['status'];
            $result['workflow'][$row['action']]['datetime'] = $row['datetime'];
        }

        return $result;
    }

    /**
     * @param ApiGetSupplierResponseRequestInterface $apiGetSupplierResponseRequest
     * @return array
     * @throws DBALException
     * @throws IWExceptionValidation
     */
    public function getSupplierResponse(ApiGetSupplierResponseRequestInterface $apiGetSupplierResponseRequest): array {
        $serpID = $apiGetSupplierResponseRequest->getSerpId();

        $supplierResponse = $this->sqlHelper->getSupplierResponse($serpID);
        if ($supplierResponse === false) {
            throw new IWExceptionValidation(sprintf(EVM::MES_SERP_ID_HAS_NEVER_BEEN_BOOKED, $serpID));
        }

        return [
            'action' => $supplierResponse['action'],
            'status' => $supplierResponse['status'],
            'datetime' => $supplierResponse['datetime'],
            'json' => $supplierResponse['json']
        ];
    }

    /**
     * @param string $serpID
     * @param OrderItem $orderItem
     * @throws GuzzleException
     * @throws IWException
     * @throws IWExceptionValidation
     */
    private function loadDataFromSearch(string $serpID, OrderItem $orderItem): void {
        $params = ApiGetRateBySerpIdRequest::createParamsArray(
            $serpID,
            false,
            true
        );

        $flightSearchAPI = new FlightSearchAPI($this->servicesConfig);
        $flightRate = $flightSearchAPI->sendRequestGerRateBySerpId($params);

        $orderItem->loadDataFromSearch($flightRate->getId(), $flightRate->getBookingToken());
    }

    /**
     * @param string $supplier
     * @param string $requestID
     * @return FlightBookingInterface
     * @throws IWException
     */
    private function getFlightBookingAdapter(string $supplier, string $requestID): FlightBookingInterface {
        if ($supplier === FlightSupplierEnumInterface::TEQUILA_BY_KIWI) {
            $visitorUniqid = $requestID;
            $numberOfBags  = 0;

            $result = new TequilaByKiwiBookingAdapter(
                $this->suppliersApiConfig,
                $numberOfBags,
                $visitorUniqid
            );
        } elseif ($supplier === FlightSupplierEnumInterface::AVIACENTER) {
            $result = new AviacenterBookingAdapter($this->suppliersApiConfig);
        } else {
            throw new IWException(sprintf(EM::MES_BOOKING_FOR_SUPPLIER_NOT_SUPPORT, $supplier));
        }
        return $result;
    }

    private function getIdOrToken(OrderItem $orderItem): string {
        $supplier = $orderItem->getSupplier();
        if ($supplier === FlightSupplierEnumInterface::TEQUILA_BY_KIWI) {
            $result = $orderItem->getBookingToken();
        } elseif ($supplier === FlightSupplierEnumInterface::AVIACENTER) {
            $result = $orderItem->getExternalId();
        } else {
            $result = '';
        }
        return $result;
    }

     /**
     * @param OrderItem $orderItem
     * @param string $action
     * @param bool $checked
     * @param array $response
     * @throws DBALException
     */
    private function saveCurrentResult(OrderItem $orderItem, string $action, bool $checked, array $response): void {
        $status = $checked ? BSE::SUCCESS : BSE::FAILURE;

        $this->sqlHelper->updateBooking(
            $orderItem->getBookingId(),
            $action,
            $checked
        );

        if (($orderItem->getPreviousStatus() !== $status) || ($orderItem->getPreviousAction() !== $action)) {
            $this->sqlHelper->insertBookingResponse(
                $orderItem->getBookingId(),
                $orderItem->getSerpID(),
                $action,
                $checked,
                $response
            );
        }

        $orderItem->loadCurrentResult($action, $status);
    }

    /**
     * @param FlightBookingInterface $flightBookingAdapter
     * @param OrderItem $orderItem
     * @return bool
     * @throws DBALException
     */
    private function actionCheck(FlightBookingInterface $flightBookingAdapter, OrderItem $orderItem): bool {
        $response = $flightBookingAdapter->checkFlights($orderItem->getPassengerCollection(), $this->getIdOrToken($orderItem));
        $checked = $flightBookingAdapter->getSimpleLogicalBookingResult($response);
        $this->saveCurrentResult($orderItem, BAE::CHECK, $checked, $response);
        return $checked;
    }

    /**
     * @param FlightBookingInterface $flightBookingAdapter
     * @param OrderItem $orderItem
     * @return bool
     * @throws DBALException
     */
    private function actionSave(FlightBookingInterface $flightBookingAdapter, OrderItem $orderItem): bool {
        $response = $flightBookingAdapter->saveBooking($orderItem->getPassengerCollection(), $this->getIdOrToken($orderItem));
        $checked = $flightBookingAdapter->getSimpleLogicalBookingResult($response);
        $this->saveCurrentResult($orderItem, BAE::SAVE, $checked, $response);
        $this->updateWebhookExternalId($flightBookingAdapter, $orderItem, $response);
        return $checked;
    }

    /**
     * @param FlightBookingInterface $flightBookingAdapter
     * @param OrderItem $orderItem
     * @param array $response
     * @throws DBALException
     */
    private function updateWebhookExternalId(FlightBookingInterface $flightBookingAdapter, OrderItem $orderItem, array $response) {
        $externalId = $flightBookingAdapter->getWebhookExternalId($response);
        if (!empty($externalId)) {
            $this->sqlHelper->updateBookingWebhookId(
                $orderItem->getBookingId(),
                $externalId
            );
        }
    }

    /**
     * @param FlightBookingInterface $flightBookingAdapter
     * @param OrderItem $orderItem
     * @return bool
     * @throws DBALException
     */
    private function actionConfirm(FlightBookingInterface $flightBookingAdapter, OrderItem $orderItem): bool {
        $json = $this->sqlHelper->getResponseJson($orderItem->getSerpID(), BAE::SAVE);
        $response = $flightBookingAdapter->confirmPayment(jsonToArray($json));
        $checked = $flightBookingAdapter->getSimpleLogicalConfirmPaymentResult($response);
        $this->saveCurrentResult($orderItem, BAE::CONFIRM, $checked, $response);
        return $checked;
    }

    /**
     * @param OrderItem $orderItem
     * @return bool
     * @throws DBALException
     */
    private function actionWaitWebhook(OrderItem $orderItem): bool {
        $supplier = $orderItem->getSupplier();
        $bookingID = $orderItem->getBookingId();

        $json = $this->webhookEngine->getWebhookJson($supplier, $bookingID);
        if (isset($json)) {
            $this->webhookEngine->setCompleted($supplier, $bookingID);
            $response = jsonToArray($json);
            $checked = true;
        } else {
            $response = [];
            $checked = false;
        }
        $this->saveCurrentResult($orderItem, BAE::WAIT_WEBHOOK, $checked, $response);
        return $checked;
    }

    /**
     * @param OrderItem $orderItem
     * @throws DBALException
     */
    private function setCompleted(OrderItem $orderItem) {
        $this->sqlHelper->updateOrderDetails($orderItem->getOrderDetailsId(), OSE::COMPLETED);
    }

    /**
     * @param OrderItem $orderItem
     * @throws DBALException
     */
    private function setCancelled(OrderItem $orderItem) {
        $this->sqlHelper->updateOrderDetails($orderItem->getOrderDetailsId(), OSE::CANCELLED);
    }

    /**
     * @param OrderItem $orderItem
     * @throws DBALException
     */
    private function setInProcess(OrderItem $orderItem) {
        $this->sqlHelper->updateOrderDetails($orderItem->getOrderDetailsId(), OSE::IN_PROCESS);
    }

    /**
     * @param OrderItem $orderItem
     * @param array $passengers
     * @throws DBALException
     * @throws GuzzleException
     * @throws IWException
     * @throws IWExceptionValidation
     */
    private function createFlightBookingProcess(OrderItem $orderItem, array $passengers) {
        $this->loadDataFromSearch($orderItem->getSerpID(), $orderItem);

        $this->sqlHelper->insertOrderDetails($orderItem->getOrderId(), $orderItem->getSerpID(), arrayToJson($passengers));
        $this->sqlHelper->loadDataFromOrderDetails($orderItem->getSerpID(), $orderItem);

        $this->sqlHelper->insertBooking(
            $orderItem->getOrderDetailsId(),
            $orderItem->getSerpID(),
            BAE::CHECK,
            false,
            $orderItem->getExternalId(),
            $orderItem->getBookingToken()
        );
    }

    /**
     * @param OrderItem $orderItem
     * @throws DBALException
     * @throws IWException
     */
    private function initFlightBookingProcess(OrderItem $orderItem) {
        $this->sqlHelper->loadDataFromOrderDetails($orderItem->getSerpID(), $orderItem);
        $this->sqlHelper->loadDataFromBooking($orderItem->getSerpID(), $orderItem);
    }

    /**
     * @throws DBALException
     * @throws IWException
     * @throws IWExceptionConfiguration
     */
    public function go() {
        $this->webhookEngine->updateReceivedWebhooks();

        $array = $this->sqlHelper->getBookingTasks();
        foreach ($array as $row) {
            $orderItem = new OrderItem($row['order_id'], $row['serp_id']);
            $this->initFlightBookingProcess($orderItem);

            $supplier = $orderItem->getSupplier();
            $flightBookingAdapter = $this->getFlightBookingAdapter($supplier, $orderItem->getRequestId());
            $action = $orderItem->getPreviousAction();
            $bookingWorkflowFlight = new BookingWorkflowFlight($supplier, $action);
            $previosAction = $action;

            $this->logging($orderItem->getSerpId(), 'Start processing', '-');
            for ($i = 0; $i < $bookingWorkflowFlight->getMaxNumberOfAttemps(); $i++) {
                if ($action == BAE::CHECK) {
                    $checked = $this->actionCheck($flightBookingAdapter, $orderItem);
                } elseif ($action == BAE::SAVE) {
                    $checked = $this->actionSave($flightBookingAdapter, $orderItem);
                } elseif ($action == BAE::CONFIRM) {
                    $checked = $this->actionConfirm($flightBookingAdapter, $orderItem);
                } elseif ($action == BAE::WAIT_WEBHOOK) {
                    $checked = $this->actionWaitWebhook($orderItem);
                } else {
                    $checked = false;
                }

                $status = $checked ? BSE::SUCCESS : BSE::FAILURE;
                $this->logging($orderItem->getSerpId(), $action, 'status '.$status);

                $action = $bookingWorkflowFlight->getNextAction($status);
                if ($previosAction == $action) {
                    sleep(3);
                }
                $previosAction = $action;
                $this->logging($orderItem->getSerpId(), 'getNextAction', "-> '".$action."'");

                if (in_array($action, [BWF::STOP_THE_EXECUTION_AND_DETERMINATE_THE_RESULT, BWF::THE_EXECUTION_IS_WAITING_FOR_THE_EVENT])) {
                    $this->logging($orderItem->getSerpId(), 'break', "action '".$action."'");
                    break;
                }
            }

            if ($bookingWorkflowFlight->getCurrentAction() === BWF::STOP_THE_EXECUTION_AND_DETERMINATE_THE_RESULT) {
                if ($bookingWorkflowFlight->getCurrentStatus() == BSE::SUCCESS) {
                    $this->setCompleted($orderItem);
                    $this->logging($orderItem->getSerpId(), "set '".OSE::COMPLETED."'", '-');
                    $this->sendBookingCompleteRequest($flightBookingAdapter, $orderItem->getSerpId());
                    $this->logging($orderItem->getSerpId(), 'sendBookingComplete', '-');
                } else {
                    $this->setCancelled($orderItem);
                    $this->logging($orderItem->getSerpId(), "set '".OSE::CANCELLED."'", '-');
                }
            } else {
                $this->setInProcess($orderItem);
                $this->logging($orderItem->getSerpId(), "set '".OSE::IN_PROCESS."'", '-');
            }

            $this->logging($orderItem->getSerpId(), 'End processing', '-');
            unset($bookingWorkflowFlight);
        }
    }

    /**
     * @param FlightBookingInterface $flightBookingAdapter
     * @param string $serpID
     * @return string
     * @throws DBALException
     */
    public function sendBookingCompleteRequest(FlightBookingInterface $flightBookingAdapter, string $serpID): string {
        $responseJson = $this->sqlHelper->getResponseJson($serpID, BAE::SAVE);
        $response = jsonToArray($responseJson);

        $webhookResponseJson = $this->sqlHelper->getResponseJson($serpID, BAE::WAIT_WEBHOOK);
        if (isset($webhookResponseJson)) {
            $webhookResponse = jsonToArray($webhookResponseJson);
        } else {
            $webhookResponse = null;
        }

        $result = "";
        $segmentCollection = $flightBookingAdapter->getFlightSegmentCollection($response);
        try {
            $clientAPI = new ClientAPI($this->servicesConfig);
            $this->logger->info('RequestSuppliersFlightBookCallback send to CliectAPI');

            $result = $clientAPI->sendRequestSuppliersFlightBookCallback(
                $serpID,
                $segmentCollection->getAssociativeArray(), //$transformation->run($segmentCollection),
                $flightBookingAdapter->getPnr($response, $webhookResponse),
                $flightBookingAdapter->getEticketLinks($response, $webhookResponse) ?? []
            );

            $this->logger->info('CliectAPI RESPONSE: '. $result);
        } catch (GuzzleException $g) {
            new ExceptionHandler($this->logger, $g);
        } finally {
            return $result;
        }
    }
}
