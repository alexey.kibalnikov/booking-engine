<?php
/**
 * ExceptionMessagesInterface.php
 * @author Alexey Kibalnikov (alexey.kibalnikov@gmail.com)
 * @copyright (c) iWeekender 2019
 * @package BookingEngine\Exceptions
 */

namespace iWeekender\BookingEngine\Exceptions;

/**
 * Runtime Error Messages (for logged).
 *
 * Runtime errors <b>will be hidden</b> from the user and the response API will not be given.
 */
interface ExceptionMessagesInterface
{
    const MES_BOOKING_FOR_SERVICE_TYPE_NOT_SUPPORT = "Booking for serviceType '%s' not support";
    const MES_BOOKING_FOR_SUPPLIER_NOT_SUPPORT = "Booking for supplier '%s' not support";
}
