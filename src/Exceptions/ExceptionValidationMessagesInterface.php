<?php
/**
 * ExceptionValidationMessagesInterface.php
 * @author Alexey Kibalnikov (alexey.kibalnikov@gmail.com)
 * @copyright (c) iWeekender 2019
 * @package BookingEngine\Exceptions
 */

namespace iWeekender\BookingEngine\Exceptions;

/**
 * Validation Error Messages (for logged and User).
 *
 * Validation errors <b>are displayed by the user</b>, because the reason for their occurrence is the user.
 */
interface ExceptionValidationMessagesInterface
{
    const MES_CREATE_NOT_UNIQUE_SERP_ID = "Booking has already been made (serpID: '%s')";
    const MES_SERP_ID_HAS_NEVER_BEEN_BOOKED = "This (serpID: '%s') has never been booked";
}
