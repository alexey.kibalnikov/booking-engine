<?php
/**
 * BookingWorkflowFlight.php
 * @author Alexey Kibalnikov (alexey.kibalnikov@gmail.com)
 * @copyright (c) iWeekender 2019
 * @package BookingEngine
 */

namespace iWeekender\BookingEngine;

use iWeekender\Contract\Booking\BookingStatusEnumInterface as BSE;
use iWeekender\Contract\IMS\BookingWorkflowFlightInterface as BWF;
use iWeekender\IMS\IMS;
use iWeekender\Exceptions\IWExceptionConfiguration;

class BookingWorkflowFlight implements BWF
{
    /**
     * @var array
     */
    private $bookingWorkflow;

    /**
     * @var string
     */
    private $currentAction;

    /**
     * @var string
     */
    private $status;

    /**
     * BookingWorkflowFlight constructor.
     * @param string $supplier
     * @param string $action
     * @throws IWExceptionConfiguration
     */
    public function __construct(string $supplier, string $action) {
        $this->bookingWorkflow = IMS::getBookingWorkflowFlight($supplier);
        $this->setCurrentAction($action);
    }

    public function getMaxNumberOfAttemps(): int {
        $result = 0;
        foreach ($this->bookingWorkflow as $action => $pipe) {
            $result += $pipe[BWF::MAX_NUMBER_OF_ATTEMPTS] === BWF::INFINITELY ? 1 : $pipe[BWF::MAX_NUMBER_OF_ATTEMPTS];
        }
        return $result;
    }

    public function getCurrentStatus(): string {
        return $this->status;
    }

    public function getCurrentAction(): string {
        return $this->currentAction;
    }

    private function setCurrentAction(string $action): void {
        $this->currentAction = $action;
    }

    private function getNextActionFromWorkflow(): string {
        return $this->bookingWorkflow[$this->currentAction][self::NEXT_ACTION];
    }

    private function getMaxNumberOfAttemptsFromWorkflow(): string {
        return $this->bookingWorkflow[$this->currentAction][self::MAX_NUMBER_OF_ATTEMPTS];
    }

    private function isInfinitely() {
        return $this->getMaxNumberOfAttemptsFromWorkflow() === self::INFINITELY;
    }

    private function countFailedAttempt() {
        $this->bookingWorkflow[$this->currentAction][self::MAX_NUMBER_OF_ATTEMPTS]--;
    }

    private function isTheLastAttempt(): bool {
        if ($this->isInfinitely()) {
            return false;
        } else {
            return $this->getMaxNumberOfAttemptsFromWorkflow() === '0';
        }
    }

    public function getNextAction(string $status): string {
        $this->status = $status;
        if ($status === BSE::SUCCESS) {
            $action = $this->getNextActionFromWorkflow();
        } else {
            if ($this->isInfinitely()) {
                $action = self::THE_EXECUTION_IS_WAITING_FOR_THE_EVENT;
            } else {
                $this->countFailedAttempt();
                if ($this->isTheLastAttempt()) {
                    $action = self::STOP_THE_EXECUTION_AND_DETERMINATE_THE_RESULT;
                } else {
                    $action = $this->getCurrentAction();
                }
            }
        }

        $this->setCurrentAction($action);
        return $action;
    }
}
