<?php
/**
 * OrderItem.php
 * @author Alexey Kibalnikov (alexey.kibalnikov@gmail.com)
 * @copyright (c) iWeekender 2019
 * @package BookingEngine
 */

namespace iWeekender\BookingEngine;

use iWeekender\BookingEngine\Exceptions\ExceptionMessagesInterface as EM;
use iWeekender\Contract\Request\ServiceTypeEnumInterface;
use iWeekender\Contract\Request\Search\SearchRequestInterface;
use iWeekender\DataModels\Request\Booking\Passengers\PassengerCollection;
use iWeekender\Exceptions\IWException;
use iWeekender\Utils\Response\ResponseHelper;

class OrderItem
{
    /**
     * @var string
     */
    private $orderID;

    /**
     * @var string
     */
    private $serpID;

    /**
     * @var string
     */
    private $serviceType;

    /**
     * @var string
     */
    private $supplier;

    /**
     * @var string
     */
    private $requestID;

    /**
     * @var int
     */
    private $orderDetailsID;

    /**
     * @var int
     */
    private $bookingID;

    /**
     * @var string
     */
    private $previousAction;

    /**
     * @var string
     */
    private $previousStatus;

    /**
     * @var string
     */
    private $externalID;

    /**
     * @var string
     */
    private $bookingToken;

    /**
     * @var PassengerCollection
     */
    private $passengerCollection;

    /**
     * OrderItem constructor.
     * @param string $orderID
     * @param string $serpID
     * @throws IWException
     */
    public function __construct(string $orderID, string $serpID) {
        $this->orderID = $orderID;
        $this->serpID = $serpID;
        $this->loadDataFromSerpIdParsing();
    }

    /**
     * @throws IWException
     */
    private function loadDataFromSerpIdParsing() {
        $parsed = ResponseHelper::parseSerpId($this->serpID);
        $this->serviceType = $parsed[SearchRequestInterface::SERVICE_TYPE];
        $this->supplier = $parsed[SearchRequestInterface::SUPPLIER];
        $this->requestID = $parsed[SearchRequestInterface::REQUEST_ID];

        if ($this->serviceType !== ServiceTypeEnumInterface::FLIGHT) {
            throw new IWException(sprintf(EM::MES_BOOKING_FOR_SERVICE_TYPE_NOT_SUPPORT, $this->serviceType));
        }
    }

    /**
     * @param string $orderDetailsID
     * @param string $passengersJson
     * @throws IWException
     */
    public function loadDataFromOrderDetails(string $orderDetailsID, string $passengersJson): void {
        $this->orderDetailsID = $orderDetailsID;

        $this->passengerCollection = new PassengerCollection();
        $this->passengerCollection->importData($passengersJson);
    }

    public function loadDataFromSearch(string $externalID, string $bookingToken): void {
        $this->externalID = $externalID;
        $this->bookingToken = $bookingToken;
    }

    public function loadDataFromBooking(string $bookingID, string $action, string $status, string $externalID, string $bookingToken): void {
        $this->bookingID      = $bookingID;
        $this->previousAction = $action;
        $this->previousStatus = $status;
        $this->externalID     = $externalID;
        $this->bookingToken   = $bookingToken;
    }

    public function loadCurrentResult(string $action, string $status): void {
        $this->previousAction = $action;
        $this->previousStatus = $status;
    }

    /**
     * @return string
     */
    public function getServiceType(): string {
        return $this->serviceType;
    }

    /**
     * @return string
     */
    public function getSupplier(): string {
        return $this->supplier;
    }

    /**
     * @return string
     */
    public function getRequestId(): string {
        return $this->requestID;
    }

    /**
     * @return string
     */
    public function getSerpId(): string {
        return $this->serpID;
    }

    /**
     * @return string
     */
    public function getOrderId(): string {
        return $this->orderID;
    }

    /**
     * @return int
     */
    public function getOrderDetailsId(): int {
        return $this->orderDetailsID;
    }

    /**
     * @return int
     */
    public function getBookingId(): int {
        return $this->bookingID;
    }

    /**
     * @return string
     */
    public function getPreviousAction(): string {
        return $this->previousAction;
    }

    /**
     * @return string
     */
    public function getPreviousStatus(): string {
        return $this->previousStatus;
    }

    /**
     * @return string
     */
    public function getExternalId(): string {
        return $this->externalID;
    }

    /**
     * @return string
     */
    public function getBookingToken(): string {
        return $this->bookingToken;
    }

    /**
     * @return PassengerCollection
     */
    public function getPassengerCollection(): PassengerCollection {
        return $this->passengerCollection;
    }

    /**
     * @param PassengerCollection $passengerCollection
     */
    public function setPassengerCollection(PassengerCollection $passengerCollection): void {
        $this->passengerCollection = $passengerCollection;
    }
}
